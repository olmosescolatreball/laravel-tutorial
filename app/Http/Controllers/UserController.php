<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Console\Input\Input;

class UserController extends Controller
{

    public function store(Request $req)
    {
        if (!$req->input('name') || !$req->input('email') || !$req->input('password')) {

            return response()->json(['errors' => array(["Status" => "fail", 'code' => 422, 'message' => 'Faltan datos necesarios para el proceso de alta.'])], 422);
            
        }
        try {
            
            $user = User::create($req->all());
            return response()->json((['status' => 'ok', 'data' => $user]), 200);

        } catch (Exception $e) {
            return response()->json(['errors' => array(array("Status" => "fail", "message" => $e))]);
        }
    }
    public function delete(Request $req)
    {
        $user = User::find($req->id);
        if (!$user) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra un usuario con ese código.'])], 404);
        }
        $res = $user->delete();
        return response()->json(['status' => 'ok', 'data' => $res, 200]);
    }
    public function update(Request $req, $idUser)
    {
        $user = User::find($idUser);
        if (!$user) {
            return response()->json(['errors' => array(['code' => 404, 'message' => 'No se encuentra un usuario con ese código.'])], 404);
        }
        if (!$req->input('name') || !$req->input('desc') || !$req->input('price')) {
            return response()->json(['errors' => array(['code' => 422, 'message' => 'Faltan datos necesarios para el proceso de alta.'])], 422);
        }
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $res = $user->save();
        return response()->json(['status' => 'ok', 'data' => $res, 200]);
    }
}
