<?php

use App\Http\Resources\UserCollection;
use App\Models\User;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Http\Resources\UserResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/*routes/api.php
* Obtenemos todos los usuarios accediendo directamente al Collection de user
*/
Route::get('/users', function () {
    return response()->json(['status' => 'ok', 'data' => new UserCollection(User::all())], 200);
});
Route::get('/users/{user}', function ($user) {
    return new UserResource(User::findOrFail($user));
});
Route::post('/user', [User::class, 'store']);

/*routes/api.php
* Llamamos al controlador que se encarga de guardar nuevos productos
*/
Route::post('/product', [ProductController::class, 'store']);
Route::put('/product', [ProductController::class, 'update']);
Route::delete('/product/{id}', [ProductController::class, 'delete']);
Route::get('/product', function () {
    return new ProductCollection(Product::all());
});
Route::get('/product/{product}', function ($product) {
    return new ProductResource(Product::findOrFail($product));
});

Route::post('/users', [UserController::class, 'store']);
Route::put('/users', [UserController::class, 'update']);
Route::delete('/users/{id}', [UserController::class, 'delete']);
